#!/usr/bin/env python3
# took 3.5 hours (please specify here how much time your solution required)
# sudo apt install python3-gmpy2
import codecs, hashlib, os, sys # do not use any other imports/libraries
import gmpy2
from secp256r1 import curve
from pyasn1.codec.der import decoder

def nb(i, length=False):
    # converts integer to bytes
    b = b''
    if length==False:
        length = (i.bit_length()+7)//8
    for _ in range(length):
        b = bytes([i & 0xff]) + b
        i >>= 8
    return b

def bn(b):
    # converts bytes to integer
    i = 0
    for char in b:
        i <<= 8
        i |= char
    return i

# --------------- asn1 DER encoder
EIGHT_SHIFT = 8
THREE_SHIFT = 3
SEVEN_BYTES_SHIFT = 1 << 7
FIVE_RIGHT_BYTES = 10 << 4
MAX_HEX = 0xFF
ZERO_PADDING = bytes([0x00])
EIGHT_ZERO_BITS_STRING = '00000000'

# Universal Tags
BOOLEAN = 0x01
INTEGER = 0x02
BIT_STRING = 0x03
OCTET_STRING = 0x04
NULL = 0x05
OBJECT_IDENTIFIER = 0x06
ENUMERATED = 0x0a
UTF8_STRING = 0x0c
SEQUENCE = 0x30
SET = 0x31
PRINTABLE_STRING = 0x13
IA5_STRING = 0x16
UTC_TIME = 0x17
UNICODE_STRING = 0x1e

# Classes
UNIVERSAL = 0x00
APPLICATION = 0x40
CONTEXT = 0x80
PRIVATE = 0xc0

# Forms
CONSTRUCTED = 0x20
PRIMITIVE = 0x00


def is_mnb_one(value_bytes):
    return value_bytes & SEVEN_BYTES_SHIFT


def len_bytes(value, shift=EIGHT_SHIFT):
    if value == 0:
        return b'\x00'
    value_bytes = b''
    while value != 0:
        value_bytes = bytes([value & ((1 << shift) - 1)]) + value_bytes
        value = value >> shift
    return value_bytes


def asn1_len(value_bytes):
    # helper function - should be used in other functions to calculate length octet(s)
    # value_bytes - bytes containing TLV value byte(s)
    # returns length (L) byte(s) for TLV

    value_length = len(value_bytes)
    b_value = len_bytes(value_length)
    if value_length < 128:
        return b_value
    else:
        return bytes([SEVEN_BYTES_SHIFT | len(b_value)]) + b_value


def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = b'\xff'
    else:
        bool = b'\x00'
    return bytes([0x01]) + asn1_len(bool) + bool

def asn1_null():
    # returns DER encoding of NULL
    return bytes([NULL | PRIMITIVE | UNIVERSAL]) + bytes([0x00])

def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    der_type = bytes([INTEGER | PRIMITIVE | UNIVERSAL])
    b_value = len_bytes(i)
    if i == 0:
        return der_type + b'\x01' + b'\x00'
    elif i > 0:
        mnb = is_mnb_one(b_value[0])
        length = asn1_len(ZERO_PADDING + b_value) if mnb else asn1_len(b_value)
        return der_type + length + ZERO_PADDING + b_value if mnb else der_type + length + b_value
    pass

def asn1_bitstring(bitstr):
    # bitstr - string containing bitstring (e.g., "10101")
    # returns DER encoding of BITSTRING
    if bitstr == '':
        return bytes([BIT_STRING | PRIMITIVE | UNIVERSAL]) + b'\x01' + b'\x00'

    bitstr_length = len(bitstr)
    missing_bits = bitstr_length - ((bitstr_length >> THREE_SHIFT) << THREE_SHIFT)

    padding = EIGHT_ZERO_BITS_STRING[missing_bits:]
    if missing_bits == 0:
        padding = ''

    bitstr = bitstr + padding
    b_str = [0]*(len(bitstr) >> 3)

    for x in range(len(b_str)):
        value = 0
        for y in range(EIGHT_SHIFT):
            value <<= 1
            if bitstr[(x << 3) + y] == '1':
                value |= 1
        b_str[x] = value

    enc_str = bytes(b_str)
    b_padding = len_bytes(len(padding))

    return bytes([BIT_STRING | PRIMITIVE | UNIVERSAL]) + asn1_len(enc_str + b_padding) + b_padding + enc_str

def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., b"abc\x01")
    # returns DER encoding of OCTETSTRING
    return bytes([OCTET_STRING | PRIMITIVE | UNIVERSAL]) + asn1_len(octets) + octets

def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    object_list = len_bytes(40 * oid[0] + oid[1])

    for value in oid[2:]:
        value_bytes = len_bytes(value, 7)
        last_bit = map(set_bit, value_bytes[: len(value_bytes) - 1])
        object_list += bytes(last_bit) + value_bytes[len(value_bytes) - 1:]
    return bytes([OBJECT_IDENTIFIER | PRIMITIVE | UNIVERSAL]) + asn1_len(object_list) + object_list

def set_bit(value):
    return value | SEVEN_BYTES_SHIFT

def asn1_sequence(der):
    # der - DER bytes to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    return bytes([SEQUENCE | PRIMITIVE | UNIVERSAL]) + asn1_len(der) + der

def asn1_set(der):
    # der - DER bytes to encapsulate into set
    # returns DER encoding of SET
    return bytes([SET | PRIMITIVE | UNIVERSAL]) + asn1_len(der) + der

def asn1_printablestring(string):
    # string - bytes containing printable characters (e.g., b"foo")
    # returns DER encoding of PrintableString
    return bytes([PRINTABLE_STRING | PRIMITIVE | UNIVERSAL]) + asn1_len(string) + string

def asn1_utctime(time):
    # time - bytes containing timestamp in UTCTime format (e.g., b"121229010100Z")
    # returns DER encoding of UTCTime
    return bytes([UTC_TIME | PRIMITIVE | UNIVERSAL]) + asn1_len(time) + time

def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    return bytes([FIVE_RIGHT_BYTES | tag]) + asn1_len(der) + der

# --------------- asn1 DER encoder end

COORDINATE_LENGTH = 32

def pem_to_der(content):
    # converts PEM content (if it is PEM) to DER
    if content[:2] == b'--':
        content = content.replace(b"-----BEGIN PUBLIC KEY-----", b"")
        content = content.replace(b"-----END PUBLIC KEY-----", b"")
        content = content.replace(b"-----BEGIN EC PRIVATE KEY-----", b"")
        content = content.replace(b"-----END EC PRIVATE KEY-----", b"")
        content = codecs.decode(content, 'base64')
    return content

def get_privkey(filename):
    # reads EC private key file and returns the private key integer (d)

    with open(filename, 'rb') as file:
        der = pem_to_der(file.read())

    decoded = decoder.decode(der)
    d = bn(bytes(decoded[0][1]))
    return d

def get_pubkey(filename):
    # reads EC public key file and returns coordinates (x, y) of the public key point
    with open(filename, 'rb') as fp:
        der = pem_to_der(fp.read())

    der_dec = decoder.decode(der)
    pub_key_bitstring = str(der_dec[0][1])
    # convert BITSTRING to bytestring
    pub_key = [0] * (len(pub_key_bitstring) >> 3)

    for x in range(len(pub_key)):
        value = 0
        for y in range(EIGHT_SHIFT):
            value <<= 1
            if pub_key_bitstring[(x << 3) + y] == '1':
                value |= 1
        pub_key[x] = value
        
    pub_key = bytes(pub_key)
    start_bit = pub_key[0]
    if start_bit == 0x04:
        pub_key = pub_key[1:]
        return [bn(pub_key[:COORDINATE_LENGTH]), bn(pub_key[COORDINATE_LENGTH:])]
    elif start_bit == 0x03 or start_bit == 0x02:
        return curve.decompress(pub_key)

    return [None, None]

def file_to_digest(filename, algorithm=hashlib.sha384):
    alg_hash = algorithm()
    with open(filename, "rb") as file:
        while True:
            new_data = file.read(512)
            if not new_data:
                break
            alg_hash.update(new_data)

    return alg_hash.digest()

def hash_file(file):
    return bn(file_to_digest(file)[:(curve.n.bit_length() + 7) // 8])

def ecdsa_sign(keyfile, filetosign, signaturefile):

    # get the private key
    d = get_privkey(keyfile)

    # calculate SHA-384 hash of the file to be signed
    # truncate the hash value to the curve size
    # convert hash to integer
    h = hash_file(filetosign)

    # generate a random nonce k in the range [1, n-1]
    val_range = (1, curve.n - 1)

    while True:
        k = bn(os.urandom((val_range[1].bit_length() + 7) // 8))

        if 1 <= k <= curve.n - 1:
            break

    # calculate ECDSA signature components r and s
    r = curve.mul(curve.g, k)[0]
    s = (gmpy2.invert(k, curve.n) * (h + r * d)) % curve.n

    # DER-encode r and s
    der = asn1_sequence(asn1_integer(r) + asn1_integer(s))

    # write DER structure to file
    with open(signaturefile, 'wb') as fp:
        fp.write(der)

def ecdsa_verify(keyfile, signaturefile, filetoverify):
    # prints "Verified OK" or "Verification failure"
    with open(signaturefile, "rb") as file:
        decoded = decoder.decode(file.read())
    r = int(decoded[0][0])
    s = int(decoded[0][1])
    s_invert = gmpy2.invert(s, curve.n)

    h = hash_file(filetoverify)

    Q = get_pubkey(keyfile)

    R = curve.add(curve.mul(curve.g, h * s_invert), curve.mul(Q, r * s_invert))

    if R[0] == r:
        print("Verified OK")
    else:
        print("Verification failure")

def usage():
    print("Usage:")
    print("sign <private key file> <file to sign> <signature output file>")
    print("verify <public key file> <signature file> <file to verify>")
    sys.exit(1)

if len(sys.argv) != 5:
    usage()
elif sys.argv[1] == 'sign':
    ecdsa_sign(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'verify':
    ecdsa_verify(sys.argv[2], sys.argv[3], sys.argv[4])
else:
    usage()
