#!/usr/bin/env python3

import argparse, hashlib, sys, datetime # do not use any other imports/libraries

# took 1.5 hours (please specify here how much time your solution required)

NAME = b'Pavel'
# parse arguments
parser = argparse.ArgumentParser(description='Proof-of-work solver')
parser.add_argument('--difficulty', default=0, type=int, help='Number of leading zero bits')
args = parser.parse_args()

def nb(i, length):
    b = [b'0'] * length

    for x in range(length, 0, -1):
        b[x - 1] = i & 0xFF
        i >>= 8

    return bytes(b)

def bn(b):
    i = 0
    for char in b:
        i <<= 8
        i |= char
    return i


bits = 8 - args.difficulty % 8
difficulty = args.difficulty // 8
nonce = 1

start = datetime.datetime.now()
digest = hashlib.sha256(hashlib.sha256(NAME + nb(0, 8)).digest()).digest()
while True:
    if not digest.startswith(bytes([0] * difficulty)) or digest[difficulty] & (0xFF >> bits) << bits != 0:
        digest = hashlib.sha256(hashlib.sha256(NAME + nb(nonce, 8)).digest()).digest()
        nonce += 1
    else:
        nonce -= 1
        break

end = datetime.datetime.now()

print("[+] Solved in " + str(((end - start).total_seconds())) + " sec ("
      + str(round(nonce / (end - start).total_seconds() / 1000000, 4)) + " Mhash/sec)")
print("[+] Input: " + (NAME + nb(nonce, 8)).hex())
print("[+] Solution: " + digest.hex())
print("[+] Nonce: " + str(nonce))
