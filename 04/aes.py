#!/usr/bin/env python3

import datetime, os, sys
from pyasn1.codec.der import decoder

# $ sudo apt-get install python3-crypto
sys.path = sys.path[1:] # removes current directory from aes.py search path
from Crypto.Cipher import AES          # https://www.dlitz.net/software/pycrypto/api/current/Crypto.Cipher.AES-module.html
from Crypto.Util.strxor import strxor  # https://www.dlitz.net/software/pycrypto/api/current/Crypto.Util.strxor-module.html#strxor
from hashlib import pbkdf2_hmac
import hashlib, hmac # do not use any other imports/libraries

# took 9.5 hours (please specify here how much time your solution required)

#==== ASN1 encoder start ====
# put your DER encoder functions here
EIGHT_SHIFT = 8
THREE_SHIFT = 3
SEVEN_BYTES_SHIFT = 1 << 7
FIVE_RIGHT_BYTES = 10 << 4
MAX_HEX = 0xFF
ZERO_PADDING = bytes([0x00])
EIGHT_ZERO_BITS_STRING = '00000000'

# Universal Tags
BOOLEAN = 0x01
INTEGER = 0x02
BIT_STRING = 0x03
OCTET_STRING = 0x04
NULL = 0x05
OBJECT_IDENTIFIER = 0x06
ENUMERATED = 0x0a
UTF8_STRING = 0x0c
SEQUENCE = 0x30
SET = 0x31
PRINTABLE_STRING = 0x13
IA5_STRING = 0x16
UTC_TIME = 0x17
UNICODE_STRING = 0x1e

# Classes
UNIVERSAL = 0x00
APPLICATION = 0x40
CONTEXT = 0x80
PRIVATE = 0xc0

# Forms
CONSTRUCTED = 0x20
PRIMITIVE = 0x00


def is_mnb_one(value_bytes):
    return value_bytes & SEVEN_BYTES_SHIFT


def len_bytes(value, shift=EIGHT_SHIFT):
    if value == 0:
        return b'\x00'
    value_bytes = b''
    while value != 0:
        value_bytes = bytes([value & ((1 << shift) - 1)]) + value_bytes
        value = value >> shift
    return value_bytes


def asn1_len(value_bytes):
    # helper function - should be used in other functions to calculate length octet(s)
    # value_bytes - bytes containing TLV value byte(s)
    # returns length (L) byte(s) for TLV

    value_length = len(value_bytes)
    b_value = len_bytes(value_length)
    if value_length < 128:
        return b_value
    else:
        return bytes([SEVEN_BYTES_SHIFT | len(b_value)]) + b_value


def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = b'\xff'
    else:
        bool = b'\x00'
    return bytes([0x01]) + asn1_len(bool) + bool

def asn1_null():
    # returns DER encoding of NULL
    return bytes([NULL | PRIMITIVE | UNIVERSAL]) + bytes([0x00])

def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    der_type = bytes([INTEGER | PRIMITIVE | UNIVERSAL])
    b_value = len_bytes(i)
    if i == 0:
        return der_type + b'\x01' + b'\x00'
    elif i > 0:
        mnb = is_mnb_one(b_value[0])
        length = asn1_len(ZERO_PADDING + b_value) if mnb else asn1_len(b_value)
        return der_type + length + ZERO_PADDING + b_value if mnb else der_type + length + b_value
    pass

def asn1_bitstring(bitstr):
    # bitstr - string containing bitstring (e.g., "10101")
    # returns DER encoding of BITSTRING
    if bitstr == '':
        return bytes([BIT_STRING | PRIMITIVE | UNIVERSAL]) + b'\x01' + b'\x00'

    bitstr_length = len(bitstr)
    missing_bits = bitstr_length - ((bitstr_length >> THREE_SHIFT) << THREE_SHIFT)

    padding = EIGHT_ZERO_BITS_STRING[missing_bits:]
    if missing_bits == 0:
        padding = ''

    bitstr = bitstr + padding
    b_str = [0]*(len(bitstr) >> 3)

    for x in range(len(b_str)):
        value = 0
        for y in range(EIGHT_SHIFT):
            value <<= 1
            if bitstr[(x << 3) + y] == '1':
                value |= 1
        b_str[x] = value

    enc_str = bytes(b_str)
    b_padding = len_bytes(len(padding))

    return bytes([BIT_STRING | PRIMITIVE | UNIVERSAL]) + asn1_len(enc_str + b_padding) + b_padding + enc_str

def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., b"abc\x01")
    # returns DER encoding of OCTETSTRING
    return bytes([OCTET_STRING | PRIMITIVE | UNIVERSAL]) + asn1_len(octets) + octets

def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    object_list = len_bytes(40 * oid[0] + oid[1])

    for value in oid[2:]:
        value_bytes = len_bytes(value, 7)
        last_bit = map(set_bit, value_bytes[: len(value_bytes) - 1])
        object_list += bytes(last_bit) + value_bytes[len(value_bytes) - 1:]
    return bytes([OBJECT_IDENTIFIER | PRIMITIVE | UNIVERSAL]) + asn1_len(object_list) + object_list

def set_bit(value):
    return value | SEVEN_BYTES_SHIFT

def asn1_sequence(der):
    # der - DER bytes to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    return bytes([SEQUENCE | PRIMITIVE | UNIVERSAL]) + asn1_len(der) + der

def asn1_set(der):
    # der - DER bytes to encapsulate into set
    # returns DER encoding of SET
    return bytes([SET | PRIMITIVE | UNIVERSAL]) + asn1_len(der) + der

def asn1_printablestring(string):
    # string - bytes containing printable characters (e.g., b"foo")
    # returns DER encoding of PrintableString
    return bytes([PRINTABLE_STRING | PRIMITIVE | UNIVERSAL]) + asn1_len(string) + string

def asn1_utctime(time):
    # time - bytes containing timestamp in UTCTime format (e.g., b"121229010100Z")
    # returns DER encoding of UTCTime
    return bytes([UTC_TIME | PRIMITIVE | UNIVERSAL]) + asn1_len(time) + time

def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    return bytes([FIVE_RIGHT_BYTES | tag]) + asn1_len(der) + der
#==== ASN1 encoder end ====

SHA1_STRING = 'sha1'
EIGHT_BYTES = 8
SIXTEEN_BYTES = 16
FOURTY_EIGHT_BYTES = 48
SHA256 = [2, 16, 840, 1, 101, 3, 4, 2, 1]
OID_AES = [2, 16, 840, 1, 101, 3, 4, 1, 2]

# this function benchmarks how many PBKDF2 iterations
# can be performed in one second on the machine it is executed
def benchmark():

    # measure time for performing 10000 iterations
    start = datetime.datetime.now()
    pbkdf2_hmac(SHA1_STRING, os.urandom(20), os.urandom(EIGHT_BYTES), 10000, FOURTY_EIGHT_BYTES)
    stop = datetime.datetime.now()
    iter = int(10000 / ((stop - start).total_seconds()))

    # extrapolate to 1 second

    print("[+] Benchmark: %s PBKDF2 iterations in 1 second" % (iter))

    return iter # returns number of iterations that can be performed in 1 second


def encrypt(pfile, cfile):
    salt = os.urandom(EIGHT_BYTES)
    iv = os.urandom(SIXTEEN_BYTES)
    # benchmarking
    iter = benchmark()

    # asking for a password
    password = input("[?] Enter password: ").encode()

    # derieving keys
    key = pbkdf2_hmac(SHA1_STRING, password, salt, iter, FOURTY_EIGHT_BYTES)
    key_hmac = key[SIXTEEN_BYTES:]
    key_aes = key[:SIXTEEN_BYTES]
    aes = AES.new(key_aes)

    # reading plaintext
    opened_file = open(pfile, 'rb')
    plain_text = opened_file.read()
    opened_file.close()

    # padding plaintext
    text_len = len(plain_text)
    padded_text = plain_text + bytes([SIXTEEN_BYTES - text_len % SIXTEEN_BYTES] * (SIXTEEN_BYTES - text_len % SIXTEEN_BYTES))

    # encrypting padded plaintext
    cipher = encrypt_padded_text(padded_text, iv, aes, len(padded_text))

    # MAC calculation (iv+ciphertext)
    digest = hmac.digest(key_hmac, iv + cipher, hashlib.sha256)

    # constructing DER header
    der = asn1_sequence(
        asn1_sequence(
            asn1_octetstring(salt) + asn1_integer(iter) + asn1_integer(48)
        ) + asn1_sequence(
            asn1_objectidentifier(OID_AES) + asn1_octetstring(iv)
        ) + asn1_sequence(
            asn1_sequence(
                asn1_objectidentifier(SHA256) + asn1_null()
            ) + asn1_octetstring(digest)
        )
    )

    # writing DER header and ciphertext to file
    text_to_write = der + cipher
    opened_file = open(cfile, 'wb')
    opened_file.write(text_to_write)
    opened_file.close()


def decrypt(cfile, pfile):

    # reading and decoding the size of DER structure
    opened_file = open(cfile, 'rb')
    der = opened_file.read()
    opened_file.close()

    head_len = bn(der[2:2 + (der[1] & (SEVEN_BYTES_SHIFT - 1)])) + (der[1] & (SEVEN_BYTES_SHIFT - 1)) + 2 if der[1] & SEVEN_BYTES_SHIFT else der[1] + 2
    decoded = decoder.decode(der[:head_len])

    # asking for a password
    password = input("[?] Enter password: ").encode()

    # derieving keys
    key = pbkdf2_hmac(SHA1_STRING, password, bytes(decoded[0][0][0]), decoded[0][0][1], FOURTY_EIGHT_BYTES)
    key_hmac = key[SIXTEEN_BYTES:]
    key_aes = key[:SIXTEEN_BYTES]
    aes = AES.new(key_aes)

    # reading ciphertext
    digest = hmac.digest(key_hmac, bytes(decoded[0][1][1]) + der[head_len:], hashlib.sha256)

    # before decryption checking MAC (iv+ciphertext)
    if hmac.compare_digest(digest, bytes(decoded[0][2][1])):
        # decrypting ciphertext
        plain_text = decrypt_padded_text(decoded, head_len, der, aes, len(der[head_len:]))

        # removing padding and writing plaintext to file
        opened_file = open(pfile, 'wb')
        opened_file.write(plain_text[: len(plain_text) - plain_text[len(plain_text) - 1]])
        opened_file.close()
    else:
        print('[-] HMAC verification failure: wrong password or modified ciphertext!')


def encrypt_padded_text(text, iv, aes, text_len):
    position = 0
    cipher_encrypted = b''
    while position < text_len:
        cipher = aes.encrypt(strxor(text[position:position + SIXTEEN_BYTES], iv))
        cipher_encrypted += cipher
        iv = cipher
        position += SIXTEEN_BYTES
    return cipher_encrypted


def decrypt_padded_text(decoded, head_len, der, aes, der_len):
    plain_text = b''
    iv = bytes(decoded[0][1][1])
    position = 0
    while position < der_len:
        plain_text += strxor(aes.decrypt(der[head_len:][position:position + SIXTEEN_BYTES]), iv)
        iv = der[head_len:][position:position + SIXTEEN_BYTES]
        position += SIXTEEN_BYTES
    return plain_text


def bn(bytes):
    # bytes - bytes to encode as integer
    # your implementation here
    if len(bytes) > 1:
        shifted_value = bytes[0]
        for value_number in range(1, len(bytes)):
            shifted_value = shifted_value << EIGHT_SHIFT
            shifted_value = shifted_value | bytes[value_number]
        return shifted_value


def usage():
    print("Usage:")
    print("-encrypt <plaintextfile> <ciphertextfile>")
    print("-decrypt <ciphertextfile> <plaintextfile>")
    sys.exit(1)


if len(sys.argv) != 4:
    usage()
elif sys.argv[1] == '-encrypt':
    encrypt(sys.argv[2], sys.argv[3])
elif sys.argv[1] == '-decrypt':
    decrypt(sys.argv[2], sys.argv[3])
else:
    usage()