#!/usr/bin/env python3

import codecs, hashlib, os, sys # do not use any other imports/libraries
from pyasn1.codec.der import decoder

# took 7.0 hours (please specify here how much time your solution required)


def nb(i, length=False):
    # converts integer to bytes
    b = b''
    if length==False:
        length = (i.bit_length()+7)//8
    for _ in range(length):
        b = bytes([i & 0xff]) + b
        i >>= 8
    return b

def bn(b):
    # converts bytes to integer
    i = 0
    for char in b:
        i <<= 8
        i |= char
    return i

#==== ASN1 encoder start ====
# put your DER encoder functions here
EIGHT_SHIFT = 8
THREE_SHIFT = 3
SEVEN_BYTES_SHIFT = 1 << 7
FIVE_RIGHT_BYTES = 10 << 4
MAX_HEX = 0xFF
ZERO_PADDING = bytes([0x00])
EIGHT_ZERO_BITS_STRING = '00000000'

# Universal Tags
BOOLEAN = 0x01
INTEGER = 0x02
BIT_STRING = 0x03
OCTET_STRING = 0x04
NULL = 0x05
OBJECT_IDENTIFIER = 0x06
ENUMERATED = 0x0a
UTF8_STRING = 0x0c
SEQUENCE = 0x30
SET = 0x31
PRINTABLE_STRING = 0x13
IA5_STRING = 0x16
UTC_TIME = 0x17
UNICODE_STRING = 0x1e

# Classes
UNIVERSAL = 0x00
APPLICATION = 0x40
CONTEXT = 0x80
PRIVATE = 0xc0

# Forms
CONSTRUCTED = 0x20
PRIMITIVE = 0x00


def is_mnb_one(value_bytes):
    return value_bytes & SEVEN_BYTES_SHIFT


def len_bytes(value, shift=EIGHT_SHIFT):
    if value == 0:
        return b'\x00'
    value_bytes = b''
    while value != 0:
        value_bytes = bytes([value & ((1 << shift) - 1)]) + value_bytes
        value = value >> shift
    return value_bytes


def asn1_len(value_bytes):
    # helper function - should be used in other functions to calculate length octet(s)
    # value_bytes - bytes containing TLV value byte(s)
    # returns length (L) byte(s) for TLV

    value_length = len(value_bytes)
    b_value = len_bytes(value_length)
    if value_length < 128:
        return b_value
    else:
        return bytes([SEVEN_BYTES_SHIFT | len(b_value)]) + b_value


def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = b'\xff'
    else:
        bool = b'\x00'
    return bytes([0x01]) + asn1_len(bool) + bool

def asn1_null():
    # returns DER encoding of NULL
    return bytes([NULL | PRIMITIVE | UNIVERSAL]) + bytes([0x00])

def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    der_type = bytes([INTEGER | PRIMITIVE | UNIVERSAL])
    b_value = len_bytes(i)
    if i == 0:
        return der_type + b'\x01' + b'\x00'
    elif i > 0:
        mnb = is_mnb_one(b_value[0])
        length = asn1_len(ZERO_PADDING + b_value) if mnb else asn1_len(b_value)
        return der_type + length + ZERO_PADDING + b_value if mnb else der_type + length + b_value
    pass

def asn1_bitstring(bitstr):
    # bitstr - string containing bitstring (e.g., "10101")
    # returns DER encoding of BITSTRING
    if bitstr == '':
        return bytes([BIT_STRING | PRIMITIVE | UNIVERSAL]) + b'\x01' + b'\x00'

    bitstr_length = len(bitstr)
    missing_bits = bitstr_length - ((bitstr_length >> THREE_SHIFT) << THREE_SHIFT)

    padding = EIGHT_ZERO_BITS_STRING[missing_bits:]
    if missing_bits == 0:
        padding = ''

    bitstr = bitstr + padding
    b_str = [0]*(len(bitstr) >> 3)

    for x in range(len(b_str)):
        value = 0
        for y in range(EIGHT_SHIFT):
            value <<= 1
            if bitstr[(x << 3) + y] == '1':
                value |= 1
        b_str[x] = value

    enc_str = bytes(b_str)
    b_padding = len_bytes(len(padding))

    return bytes([BIT_STRING | PRIMITIVE | UNIVERSAL]) + asn1_len(enc_str + b_padding) + b_padding + enc_str

def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., b"abc\x01")
    # returns DER encoding of OCTETSTRING
    return bytes([OCTET_STRING | PRIMITIVE | UNIVERSAL]) + asn1_len(octets) + octets

def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    object_list = len_bytes(40 * oid[0] + oid[1])

    for value in oid[2:]:
        value_bytes = len_bytes(value, 7)
        last_bit = map(set_bit, value_bytes[: len(value_bytes) - 1])
        object_list += bytes(last_bit) + value_bytes[len(value_bytes) - 1:]
    return bytes([OBJECT_IDENTIFIER | PRIMITIVE | UNIVERSAL]) + asn1_len(object_list) + object_list

def set_bit(value):
    return value | SEVEN_BYTES_SHIFT

def asn1_sequence(der):
    # der - DER bytes to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    return bytes([SEQUENCE | PRIMITIVE | UNIVERSAL]) + asn1_len(der) + der

def asn1_set(der):
    # der - DER bytes to encapsulate into set
    # returns DER encoding of SET
    return bytes([SET | PRIMITIVE | UNIVERSAL]) + asn1_len(der) + der

def asn1_printablestring(string):
    # string - bytes containing printable characters (e.g., b"foo")
    # returns DER encoding of PrintableString
    return bytes([PRINTABLE_STRING | PRIMITIVE | UNIVERSAL]) + asn1_len(string) + string

def asn1_utctime(time):
    # time - bytes containing timestamp in UTCTime format (e.g., b"121229010100Z")
    # returns DER encoding of UTCTime
    return bytes([UTC_TIME | PRIMITIVE | UNIVERSAL]) + asn1_len(time) + time

def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    return bytes([FIVE_RIGHT_BYTES | tag]) + asn1_len(der) + der
#==== ASN1 encoder end ====

SHA256 = [2, 16, 840, 1, 101, 3, 4, 2, 1]

PEM_HEAD_PRIV = b'-----BEGIN RSA PRIVATE KEY-----\n'
PEM_HEAD_PUB = b'-----BEGIN PUBLIC KEY-----\n'
PEM_TAIL_PRIV = b'\n-----END RSA PRIVATE KEY-----\n'
PEM_TAIL_PUB = b'\n-----END PUBLIC KEY-----\n'

def is_pem(content):
    return PEM_HEAD_PUB in content or PEM_HEAD_PRIV in content

def pem_to_der(content):
    # converts PEM content to DER
    if is_pem(content):
        if PEM_HEAD_PUB in content:
            content = content.replace(PEM_HEAD_PUB, b'').replace(PEM_TAIL_PUB, b'')
        elif PEM_HEAD_PRIV in content:
            content = content.replace(PEM_HEAD_PRIV, b'').replace(PEM_TAIL_PRIV, b'')
        content = codecs.decode(content, 'base64')
    return content


def get_pubkey(filename):
    # reads public key file and returns (n, e)
    openfile = open(filename, 'rb')
    content = openfile.read()
    openfile.close()

    content = pem_to_der(content)

    # decode the DER to get public key DER structure, which is encoded as BITSTRING
    decoded = decoder.decode(content)
    pub_key_bitstring = str(decoded[0][1])

    # convert BITSTRING to bytestring
    pub_key = [0]*(len(pub_key_bitstring) >> 3)

    for x in range(len(pub_key)):
        value = 0
        for y in range(EIGHT_SHIFT):
            value <<= 1
            if pub_key_bitstring[(x << 3) + y] == '1':
                value |= 1
        pub_key[x] = value

    # decode the bytestring (which is actually DER) and return (n, e)

    pub_key_decoded = decoder.decode(bytes(pub_key))

    return int(pub_key_decoded[0][0]), int(pub_key_decoded[0][1])

def get_privkey(filename):
    # reads private key file and returns (n, d)
    openfile = open(filename, 'rb')
    content = openfile.read()
    openfile.close()

    content = pem_to_der(content)

    decoded_priv_key = decoder.decode(content)

    return int(decoded_priv_key[0][1]), int(decoded_priv_key[0][3])

def create_padding(padding_len):
    ps = b''
    while len(ps) != padding_len:
        new_byte = os.urandom(1)
        if new_byte[0] != 0x00:
            ps += new_byte
    return ps

def pkcsv15pad_encrypt(plaintext, n):
    # pad plaintext for encryption according to PKCS#1 v1.5

    # calculate number of bytes required to represent the modulus n
    n_len = (n.bit_length() + 7) // 8

    # plaintext must be at least 11 bytes smaller than the modulus
    plaintext_len = len(plaintext)
    if plaintext_len < n_len - 11:
        # generate padding bytes
        padding_len = n_len - plaintext_len - 3
        ps = create_padding(padding_len)

        padded_plaintext = bytes([0x00, 0x02]) + ps + bytes([0x00]) + plaintext
        return padded_plaintext
    print("Error")

def pkcsv15pad_sign(plaintext, n):
    # pad plaintext for signing according to PKCS#1 v1.5

    # calculate byte length of modulus n
    n_len = (n.bit_length() + 7) // 8

    # plaintext must be at least 3 bytes smaller than modulus
    plaintext_len = len(plaintext)
    if plaintext_len < n_len - 3:
        # generate padding bytes
        padding_len = n_len - plaintext_len - 3
        ps = [0xFF] * padding_len

        padded_plaintext = bytes([0x00, 0x01]) + bytes(ps) + bytes([0x00]) + plaintext
        return padded_plaintext
    print("Error")

def pkcsv15pad_remove(plaintext):
    # removes PKCS#1 v1.5 padding
    return plaintext[plaintext[1:].find(0x00) + 2:]

def encrypt(keyfile, plaintextfile, ciphertextfile):
    n, e = get_pubkey(keyfile)

    open_file = open(plaintextfile, 'rb')
    plain_text = pkcsv15pad_encrypt(open_file.read(), n)
    open_file.close()

    m = bn(plain_text)
    c = nb(pow(m, e, n))

    open_file = open(ciphertextfile, 'wb')
    open_file.write(c)
    open_file.close()
    pass

def decrypt(keyfile, ciphertextfile, plaintextfile):
    open_file = open(ciphertextfile, 'rb')
    c = bn(open_file.read())
    open_file.close()

    n, d = get_privkey(keyfile)

    plain_text = pkcsv15pad_remove(nb(pow(c, d, n)))

    open_file = open(plaintextfile, 'wb')
    open_file.write(plain_text)
    open_file.close()
    pass

def file_to_digest(filename, algorithm=hashlib.sha256):
    file = open(filename, "rb")

    alg_hash = algorithm()

    while True:
        new_data = file.read(512)
        if not new_data:
            break
        alg_hash.update(new_data)

    file.close()
    return alg_hash.digest()

def digestinfo_der(filename):
    # returns ASN.1 DER encoded DigestInfo structure containing SHA256 digest of file
    digest = file_to_digest(filename)
    der = asn1_sequence(
        asn1_sequence(
            asn1_objectidentifier(SHA256) + asn1_null()
        ) + asn1_octetstring(digest)
    )
    return der

def sign(keyfile, filetosign, signaturefile):
    der = digestinfo_der(filetosign)

    n, d = get_privkey(keyfile)
    m = bn(pkcsv15pad_sign(der, n))
    s = nb(pow(m, d, n), (n.bit_length() + 7) // 8)

    opened_file = open(signaturefile, 'wb')
    opened_file.write(s)
    opened_file.close()

    # Warning: make sure that signaturefile produced has the same
    # length as the modulus (hint: use parametrized nb()).

def verify(keyfile, signaturefile, filetoverify):
    # prints "Verified OK" or "Verification failure"
    n, e = get_pubkey(keyfile)

    opened_file = open(signaturefile, 'rb')
    s = bn(opened_file.read())
    opened_file.close()

    m = pkcsv15pad_remove(nb(pow(s, e, n)))
    der = digestinfo_der(filetoverify)

    print('Verified OK') if der == m else print('Verification failure')


def usage():
    print("Usage:")
    print("encrypt <public key file> <plaintext file> <output ciphertext file>")
    print("decrypt <private key file> <ciphertext file> <output plaintext file>")
    print("sign <private key file> <file to sign> <signature output file>")
    print("verify <public key file> <signature file> <file to verify>")
    sys.exit(1)

if len(sys.argv) != 5:
    usage()
elif sys.argv[1] == 'encrypt':
    encrypt(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'decrypt':
    decrypt(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'sign':
    sign(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'verify':
    verify(sys.argv[2], sys.argv[3], sys.argv[4])
else:
    usage()
