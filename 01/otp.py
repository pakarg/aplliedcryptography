#!/usr/bin/env python3
import os, sys       # do not use any other imports/libraries
# took x.y hours (please specify here how much time your solution required)
# took 6.5 hours. Had problems with encryption and decryption examples
BYTES_SHIFT = 8
BYTES_ONES = 0b11111111


def bn(bytes):
    # bytes - bytes to encode as integer
    # your implementation here
    if len(bytes) > 1:
        shifted_value = bytes[0]
        for value_number in range(1, len(bytes)):
            shifted_value = shifted_value << BYTES_SHIFT
            shifted_value = shifted_value | bytes[value_number]
        return shifted_value


def nb(i, length):
    # i - integer to encode as bytes
    # length - specifies in how many bytes the number should be encoded
    # your implementation here
    array_from_integer = ''
    for x in range(length):
        array_from_integer = (str(chr(i & BYTES_ONES))) + array_from_integer
        i = i >> BYTES_SHIFT

    return bytes(array_from_integer, 'utf8')


def encrypt(pfile, kfile, cfile):
    # your implementation here
    opened_file = open(pfile, 'rb').read()
    file_length = len(opened_file)
    key = os.urandom(file_length)

    plaintext_integer = bn(opened_file)
    key_integer = bn(key)

    ciphertext = plaintext_integer ^ key_integer
    ciphertext_bytes = nb(ciphertext, file_length)

    write_to_file(kfile, key)
    write_to_file(cfile, ciphertext_bytes)
    pass


def decrypt(cfile, kfile, pfile):
    # your implementation here
    opened_file = open(cfile, 'rb').read()
    opened_file_key = open(kfile, 'rb').read()
    key_integer = bn(opened_file_key)
    file_length = len(opened_file)

    cipher_integer = bn(opened_file)

    plaintext = cipher_integer ^ key_integer
    plaintext_bytes = nb(plaintext, file_length)

    write_to_file(pfile, plaintext_bytes)
    pass


def write_to_file(file_name, to_write):
    cipher_file = open(file_name, "wb")
    cipher_file.write(to_write)
    cipher_file.close()


def usage():
    print("Usage:")
    print("encrypt <plaintext file> <output key file> <ciphertext output file>")
    print("decrypt <ciphertext file> <key file> <plaintext output file>")
    sys.exit(1)

if len(sys.argv) != 5:
    usage()
elif sys.argv[1] == 'encrypt':
    encrypt(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'decrypt':
    decrypt(sys.argv[2], sys.argv[3], sys.argv[4])
else:
    usage()
