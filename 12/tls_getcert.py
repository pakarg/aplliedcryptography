#!/usr/bin/env python3

import argparse, codecs, datetime, os, socket, sys, time # do not use any other imports/libraries
from urllib.parse import urlparse

# took 2.5 hours (please specify here how much time your solution required)

# parse arguments
parser = argparse.ArgumentParser(description='TLS v1.2 client')
parser.add_argument('url', type=str, help='URL to request')
parser.add_argument('--certificate', type=str, help='File to write PEM-encoded server certificate')
args = parser.parse_args()

def nb(i, length=False):
    # converts integer to bytes
    b = b''
    if length==False:
        length = (i.bit_length()+7)//8
    for _ in range(length):
        b = bytes([i & 0xff]) + b
        i >>= 8
    return b

def bn(b):
    # converts bytes to integer
    i = 0
    for byte in b:
        i <<= 8
        i |= byte
    return i

# returns TLS record that contains ClientHello Handshake message
def client_hello():

    print("--> ClientHello()")

    # list of cipher suites the client supports
    csuite = b"\x00\x05" # TLS_RSA_WITH_RC4_128_SHA
    csuite+= b"\x00\x2f" # TLS_RSA_WITH_AES_128_CBC_SHA
    csuite+= b"\x00\x35" # TLS_RSA_WITH_AES_256_CBC_SHA

    # add Handshake message header
    ran_time = nb(int(time.time()), 4) + os.urandom(28)
    csuite_len = nb(len(csuite), 2)
    header = b"\x03\x03" + ran_time + b'\x00' + csuite_len

    # add record layer header
    body =  header + csuite + b'\x01\x00'
    data = b'\x01' + nb(len(body), 3) + body
    record = b'\x16' + b"\x03\x03" + nb(len(data), 2) + data
    return record

# returns TLS record that contains 'Certificate unknown' fatal Alert message
def alert():
    print("--> Alert()")

    # add alert message
    message = nb(len(b'\x02\x2e'), 2) + b'\x02\x2e'

    # add record layer header
    record = b'\x15' + b"\x03\x03" + message
    return record

# parse TLS Handshake messages
def parsehandshake(r):
    global server_hello_done_received

    # read Handshake message type and length from message header
    length = bn(r[1:4])
    message = r[4:4 + length]
    htype = r[0:1]

    if htype == b"\x02":
        print("	<--- ServerHello()")

        server_random = message[2:34]
        print("	[+] server randomness:", server_random.hex().upper())

        gmt = datetime.datetime.fromtimestamp(bn(server_random[:4])).strftime('%Y-%m-%d %H:%M:%S')
        print("	[+] server timestamp:", gmt)

        sessid_len = message[34]
        sessid = message[35:35 + sessid_len]
        print("	[+] TLS session ID:", sessid.hex().upper())

        cipher = message[35 + sessid_len:37 + sessid_len]
        if cipher==b"\x00\x2f":
            print("	[+] Cipher suite: TLS_RSA_WITH_AES_128_CBC_SHA")
        elif cipher==b"\x00\x35":
            print("	[+] Cipher suite: TLS_RSA_WITH_AES_256_CBC_SHA")
        elif cipher==b"\x00\x05":
            print("	[+] Cipher suite: TLS_RSA_WITH_RC4_128_SHA")
        else:
            print("[-] Unsupported cipher suite selected:", cipher.hex())
            sys.exit(1)

        compression = message[37 + sessid_len:38 + sessid_len]
        if compression!=b"\x00":
            print("[-] Wrong compression:", compression.hex())
            sys.exit(1)

    elif htype == b"\x0b":
        certlen = bn(message[3:6])
        print("	<--- Certificate()")
        print("	[+] Server certificate length:", certlen)
        if args.certificate:
            print("	[+] Server certificate saved in:", args.certificate)
            with open(args.certificate, 'wb') as file:
                to_write = b"-----BEGIN CERTIFICATE-----\n"
                to_write += codecs.encode(message[6:6 + certlen], 'base64')
                to_write += b"-----END CERTIFICATE-----\n"
                file.write(to_write)
    elif htype == b"\x0e":
        print("	<--- ServerHelloDone()")
        server_hello_done_received = True
    else:
        print("[-] Unknown Handshake type:", htype.hex())
        sys.exit(1)

    # handle the case of several Handshake messages in one record
    leftover = r[4 + length:]
    if len(leftover):
        parsehandshake(leftover)

# parses TLS record
def parserecord(r):
    # parse TLS record header and pass the record body to the corresponding parsing method (i.e., parsehandshake())
    if r[0] == 0x16:
        print("<--- Handshake()")
        parsehandshake(r[5:])

# read from the socket full TLS record
def readrecord():
    global s

    record = b""

    # read the TLS record header (5 bytes)
    body = b''
    while True:
        body += readb()
        if len(body) >= 5:
            break
    record += body

    # find data length
    length = bn(record[3:])

    # read the TLS record body
    body = b''
    while True:
        body += readb()
        if len(body) >= length:
            break
    record += body

    return record

def readb():
    b = s.recv(1)

    if b == b'':
        exit(1)

    return b

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
url = urlparse(args.url)
host = url.netloc.split(':')
if len(host) > 1:
    port = int(host[1])
else:
    port = 443
host = host[0]
path = url.path

s.connect((host, port))
s.send(client_hello())

server_hello_done_received = False
while not server_hello_done_received:
    parserecord(readrecord())
s.send(alert())

print("[+] Closing TCP connection!")
s.close()
