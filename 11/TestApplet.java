package appcrypto;

import javacard.framework.*;
import javacard.security.*;
import javacardx.crypto.*;

// took 3.5 hours (please specify here how much time your solution required)

public class TestApplet extends Applet {
	
	private KeyPair keypair;
	private RSAPublicKey pub;
	private Cipher rsa;
	
	public static void install(byte[] ba, short offset, byte len) {
		(new TestApplet()).register();
	}

	private TestApplet() {
	}
	
	public void process(APDU apdu) {
		byte[] buf = apdu.getBuffer();
		
		switch (buf[ISO7816.OFFSET_INS]) {
			case (byte) 0x02: {
				if (buf[ISO7816.OFFSET_LC] != 0x00) {
					ISOException.throwIt(ISO7816.SW_DATA_INVALID);
				} else if (keypair == null && rsa == null && pub == null) {
					generateKeys();
				}
				return;
			} case (byte) 0x04: {
				checkLc(buf[ISO7816.OFFSET_LC]);

				apdu.setOutgoingAndSend((short) 0x00, pub.getExponent(buf, (short) 0x00));
				return;
			} case (byte) 0x06: {
				checkLc(buf[ISO7816.OFFSET_LC]);

				apdu.setOutgoingAndSend((short) 0x00, pub.getModulus(buf, (short) 0x00));
				return;
			} case (byte) 0x08: {
				if (rsa == null) {
					ISOException.throwIt(ISO7816.SW_CONDITIONS_NOT_SATISFIED);
				}

				short len = (short) (buf[ISO7816.OFFSET_LC] & 0xFF);
				apdu.setIncomingAndReceive();
				Util.arrayCopyNonAtomic(buf, (short) ISO7816.OFFSET_CDATA, buf, (short) ISO7816.OFFSET_LC, len);

				apdu.setOutgoingAndSend((short) 0x00, rsa.doFinal(buf, (short) ISO7816.OFFSET_P1, (short) (len + 2), buf, (short) 0x00));
				return;
			}
		}

		ISOException.throwIt(ISO7816.SW_INS_NOT_SUPPORTED);		
	}
	
	private void generateKeys() {
		keypair = new KeyPair(KeyPair.ALG_RSA, KeyBuilder.LENGTH_RSA_2048);
		keypair.genKeyPair();

		rsa = Cipher.getInstance(Cipher.ALG_RSA_PKCS1, false);
		rsa.init(keypair.getPrivate(), Cipher.MODE_DECRYPT);

		pub = (RSAPublicKey) keypair.getPublic();
	}

	private void checkLc(byte lc) {
		if (lc != 0x00) {
			ISOException.throwIt(ISO7816.SW_DATA_INVALID);
		} else if (pub == null) {
			ISOException.throwIt(ISO7816.SW_CONDITIONS_NOT_SATISFIED);
		}
	}

}
