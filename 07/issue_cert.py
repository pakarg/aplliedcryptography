#!/usr/bin/env python3

import argparse, codecs, hashlib, os, sys # do not use any other imports/libraries
from pyasn1.codec.der import decoder, encoder

# took 6.0 hours (please specify here how much time your solution required)


# parse arguments
parser = argparse.ArgumentParser(description='issue TLS server certificate based on CSR', add_help=False)
parser.add_argument("CA_cert_file", help="CA certificate (in PEM or DER form)")
parser.add_argument("CA_private_key_file", help="CA private key (in PEM or DER form)")
parser.add_argument("csr_file", help="CSR file (in PEM or DER form)")
parser.add_argument("output_cert_file", help="File to store certificate (in PEM form)")
args = parser.parse_args()

def nb(i, length=False):
    # converts integer to bytes
    b = b''
    if length==False:
        length = (i.bit_length()+7)//8
    for _ in range(length):
        b = bytes([i & 0xff]) + b
        i >>= 8
    return b

def bn(b):
    # converts bytes to integer
    i = 0
    for byte in b:
        i <<= 8
        i |= byte
    return i

#==== ASN1 encoder start ====
# put your DER encoder functions here
EIGHT_SHIFT = 8
THREE_SHIFT = 3
SEVEN_BYTES_SHIFT = 1 << 7
FIVE_RIGHT_BYTES = 10 << 4
MAX_HEX = 0xFF
ZERO_PADDING = bytes([0x00])
EIGHT_ZERO_BITS_STRING = '00000000'

# Universal Tags
BOOLEAN = 0x01
INTEGER = 0x02
BIT_STRING = 0x03
OCTET_STRING = 0x04
NULL = 0x05
OBJECT_IDENTIFIER = 0x06
ENUMERATED = 0x0a
UTF8_STRING = 0x0c
SEQUENCE = 0x30
SET = 0x31
PRINTABLE_STRING = 0x13
IA5_STRING = 0x16
UTC_TIME = 0x17
UNICODE_STRING = 0x1e

# Classes
UNIVERSAL = 0x00
APPLICATION = 0x40
CONTEXT = 0x80
PRIVATE = 0xc0

# Forms
CONSTRUCTED = 0x20
PRIMITIVE = 0x00


def is_mnb_one(value_bytes):
    return value_bytes & SEVEN_BYTES_SHIFT


def len_bytes(value, shift=EIGHT_SHIFT):
    if value == 0:
        return b'\x00'
    value_bytes = b''
    while value != 0:
        value_bytes = bytes([value & ((1 << shift) - 1)]) + value_bytes
        value = value >> shift
    return value_bytes


def asn1_len(value_bytes):
    # helper function - should be used in other functions to calculate length octet(s)
    # value_bytes - bytes containing TLV value byte(s)
    # returns length (L) byte(s) for TLV

    value_length = len(value_bytes)
    b_value = len_bytes(value_length)
    if value_length < 128:
        return b_value
    else:
        return bytes([SEVEN_BYTES_SHIFT | len(b_value)]) + b_value


def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = b'\xff'
    else:
        bool = b'\x00'
    return bytes([0x01]) + asn1_len(bool) + bool

def asn1_null():
    # returns DER encoding of NULL
    return bytes([NULL | PRIMITIVE | UNIVERSAL]) + bytes([0x00])

def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    der_type = bytes([INTEGER | PRIMITIVE | UNIVERSAL])
    b_value = len_bytes(i)
    if i == 0:
        return der_type + b'\x01' + b'\x00'
    elif i > 0:
        mnb = is_mnb_one(b_value[0])
        length = asn1_len(ZERO_PADDING + b_value) if mnb else asn1_len(b_value)
        return der_type + length + ZERO_PADDING + b_value if mnb else der_type + length + b_value
    pass

def asn1_bitstring(bitstr):
    # bitstr - string containing bitstring (e.g., "10101")
    # returns DER encoding of BITSTRING
    if bitstr == '':
        return bytes([BIT_STRING | PRIMITIVE | UNIVERSAL]) + b'\x01' + b'\x00'

    bitstr_length = len(bitstr)
    missing_bits = bitstr_length - ((bitstr_length >> THREE_SHIFT) << THREE_SHIFT)

    padding = EIGHT_ZERO_BITS_STRING[missing_bits:]
    if missing_bits == 0:
        padding = ''

    bitstr = bitstr + padding
    b_str = [0]*(len(bitstr) >> 3)

    for x in range(len(b_str)):
        value = 0
        for y in range(EIGHT_SHIFT):
            value <<= 1
            if bitstr[(x << 3) + y] == '1':
                value |= 1
        b_str[x] = value

    enc_str = bytes(b_str)
    b_padding = len_bytes(len(padding))

    return bytes([BIT_STRING | PRIMITIVE | UNIVERSAL]) + asn1_len(enc_str + b_padding) + b_padding + enc_str

def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., b"abc\x01")
    # returns DER encoding of OCTETSTRING
    return bytes([OCTET_STRING | PRIMITIVE | UNIVERSAL]) + asn1_len(octets) + octets

def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    object_list = len_bytes(40 * oid[0] + oid[1])

    for value in oid[2:]:
        value_bytes = len_bytes(value, 7)
        last_bit = map(set_bit, value_bytes[: len(value_bytes) - 1])
        object_list += bytes(last_bit) + value_bytes[len(value_bytes) - 1:]
    return bytes([OBJECT_IDENTIFIER | PRIMITIVE | UNIVERSAL]) + asn1_len(object_list) + object_list

def set_bit(value):
    return value | SEVEN_BYTES_SHIFT

def asn1_sequence(der):
    # der - DER bytes to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    return bytes([SEQUENCE | PRIMITIVE | UNIVERSAL]) + asn1_len(der) + der

def asn1_set(der):
    # der - DER bytes to encapsulate into set
    # returns DER encoding of SET
    return bytes([SET | PRIMITIVE | UNIVERSAL]) + asn1_len(der) + der

def asn1_printablestring(string):
    # string - bytes containing printable characters (e.g., b"foo")
    # returns DER encoding of PrintableString
    return bytes([PRINTABLE_STRING | PRIMITIVE | UNIVERSAL]) + asn1_len(string) + string

def asn1_utctime(time):
    # time - bytes containing timestamp in UTCTime format (e.g., b"121229010100Z")
    # returns DER encoding of UTCTime
    return bytes([UTC_TIME | PRIMITIVE | UNIVERSAL]) + asn1_len(time) + time

def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    return bytes([FIVE_RIGHT_BYTES | tag]) + asn1_len(der) + der
#==== ASN1 encoder end ====

CN = [2, 5, 4, 3]
SIGN = [1, 2, 840, 113549, 1, 1, 11]
SHA256 = [2, 16, 840, 1, 101, 3, 4, 2, 1]


def pem_to_der(content):
    # converts PEM content (if it is PEM) to DER
    if content[:2] == b'--':
        content = content.replace(b"-----BEGIN CERTIFICATE REQUEST-----", b"")
        content = content.replace(b"-----END CERTIFICATE REQUEST-----", b"")
        content = content.replace(b"-----BEGIN CERTIFICATE-----", b"")
        content = content.replace(b"-----END CERTIFICATE-----", b"")
        content = content.replace(b"-----BEGIN PUBLIC KEY-----", b"")
        content = content.replace(b"-----END PUBLIC KEY-----", b"")
        content = content.replace(b"-----BEGIN RSA PRIVATE KEY-----", b"")
        content = content.replace(b"-----END RSA PRIVATE KEY-----", b"")
        content = codecs.decode(content, 'base64')
    return content

def get_privkey(filename):
    # reads RSA private key file and returns (n, d)
    with open(filename, 'rb') as file:
        privkey = decoder.decode(pem_to_der(file.read()))

    return int(privkey[0][1]), int(privkey[0][3])


def pkcsv15pad_sign(plaintext, n):
    # pad plaintext for signing according to PKCS#1 v1.5

    # calculate byte length of modulus n
    n_len = (n.bit_length() + 7) // 8

    # plaintext must be at least 3 bytes smaller than modulus
    plaintext_len = len(plaintext)
    if plaintext_len < n_len - 3:
        # add padding bytes
        padding_len = n_len - plaintext_len - 3
        ps = [0xFF] * padding_len

        return bytes([0x00, 0x01]) + bytes(ps) + bytes([0x00]) + plaintext
    print("Error")


def digestinfo_der(m):
    # returns ASN.1 DER-encoded DigestInfo structure containing SHA256 digest of m
    digest = hashlib.sha256(m).digest()

    der = asn1_sequence(
        asn1_sequence(
            asn1_objectidentifier(SHA256) + asn1_null()
        ) + asn1_octetstring(digest)
    )
    return der


def sign(m, keyfile):
    # signs DigestInfo of message m
    der = digestinfo_der(m)

    n, d = get_privkey(keyfile)
    m = bn(pkcsv15pad_sign(der, n))
    signature = nb(pow(m, d, n), (n.bit_length() + 7) // 8)

    return signature


def get_subject_cn(csr_der):
    # returns CommonName value from CSR's Distinguished Name field
    common_name = decoder.decode(csr_der)[0][0][1]

    # looping over Distinguished Name entries until CN found
    for x in range(len(common_name)):
        num_map = map(int, str(common_name[x][0][0]).split("."))
        num_list = list(num_map)
        if num_list == CN:
            return common_name[x][0][1]

def get_subjectPublicKeyInfo(csr_der):
    # returns DER-encoded subjectPublicKeyInfo from CSR
    return encoder.encode(decoder.decode(csr_der)[0][0][2])

def get_subjectName(cert_der):
    # returns DER-encoded subject name from CA certificate
    return encoder.encode(decoder.decode(cert_der)[0][0][5])


def issue_certificate(private_key_file, issuer, subject, pubkey):
    # receives CA private key filename, DER-encoded CA Distinguished Name, self-constructed DER-encoded subject's Distinguished Name and DER-encoded subjectPublicKeyInfo
    # returns X.509v3 certificate in PEM format

    tbs_cert = asn1_sequence(
        asn1_tag_explicit(asn1_integer(2), 0) +
        asn1_integer(1234567890) +
        asn1_sequence(asn1_objectidentifier(SIGN)) + issuer +
        asn1_sequence(
            asn1_utctime(b"210101000000Z") +
            asn1_utctime(b"211201235959Z")
        ) + subject + pubkey +
        asn1_tag_explicit(asn1_sequence(
            asn1_sequence(
                asn1_objectidentifier([2, 5, 29, 19]) +
                asn1_boolean(True) + asn1_octetstring(asn1_sequence(asn1_boolean(False)))) +
            asn1_sequence(
                asn1_objectidentifier([2, 5, 29, 15]) +
                asn1_boolean(True) + asn1_octetstring(asn1_bitstring('1'))) +
            asn1_sequence(
                asn1_objectidentifier([2, 5, 29, 37]) +
                asn1_boolean(True) + asn1_octetstring(asn1_sequence(asn1_objectidentifier([1, 3, 6, 1, 5, 5, 7, 3, 1]))))
        ), 3)
    )

    enc = bytes([0x00]) + sign(tbs_cert, private_key_file)

    cert = asn1_sequence(
        tbs_cert +
        asn1_sequence(asn1_objectidentifier(SIGN)) +
        (bytes([0x03]) + asn1_len(enc) + enc)
    )

    pem = b"-----BEGIN CERTIFICATE-----\n" + codecs.encode(cert, 'base64') + b"-----END CERTIFICATE-----\n"

    return pem

# obtain subject's CN from CSR
csr_der = pem_to_der(open(args.csr_file, 'rb').read())
subject_cn_text = get_subject_cn(csr_der)

print("[+] Issuing certificate for \"%s\"" % (subject_cn_text))

# obtain subjectPublicKeyInfo from CSR
pubkey = get_subjectPublicKeyInfo(csr_der)

# construct subject name DN for end-entity's certificate
subject = asn1_sequence(
    asn1_set(
        asn1_sequence(
            asn1_objectidentifier(CN) +
            asn1_printablestring(str(subject_cn_text).encode('utf-8'))
        )
    )
)

# get subject name DN from CA certificate
CAcert = pem_to_der(open(args.CA_cert_file, 'rb').read())
CAsubject = get_subjectName(CAcert)

# issue certificate
cert_pem = issue_certificate(args.CA_private_key_file, CAsubject, subject, pubkey)
open(args.output_cert_file, 'wb').write(cert_pem)
