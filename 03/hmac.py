#!/usr/bin/env python3

import codecs, hashlib, sys
from pyasn1.codec.der import decoder
sys.path = sys.path[1:] # don't remove! otherwise the library import below will try to import your hmac2.py
import hmac # do not use any other imports/libraries

# took 5.0 hours (please specify here how much time your solution required)

#==== ASN1 encoder start ====
# put your DER encoder functions here
EIGHT_SHIFT = 8
THREE_SHIFT = 3
SEVEN_BYTES_SHIFT = 1 << 7
FIVE_RIGHT_BYTES = 10 << 4
MAX_HEX = 0xFF
ZERO_PADDING = bytes([0x00])
EIGHT_ZERO_BITS_STRING = '00000000'

# Universal Tags
BOOLEAN = 0x01
INTEGER = 0x02
BIT_STRING = 0x03
OCTET_STRING = 0x04
NULL = 0x05
OBJECT_IDENTIFIER = 0x06
ENUMERATED = 0x0a
UTF8_STRING = 0x0c
SEQUENCE = 0x30
SET = 0x31
PRINTABLE_STRING = 0x13
IA5_STRING = 0x16
UTC_TIME = 0x17
UNICODE_STRING = 0x1e

# Classes
UNIVERSAL = 0x00
APPLICATION = 0x40
CONTEXT = 0x80
PRIVATE = 0xc0

# Forms
CONSTRUCTED = 0x20
PRIMITIVE = 0x00


def is_mnb_one(value_bytes):
    return value_bytes & SEVEN_BYTES_SHIFT


def len_bytes(value, shift=EIGHT_SHIFT):
    if value == 0:
        return b'\x00'
    value_bytes = b''
    while value != 0:
        value_bytes = bytes([value & ((1 << shift) - 1)]) + value_bytes
        value = value >> shift
    return value_bytes


def asn1_len(value_bytes):
    # helper function - should be used in other functions to calculate length octet(s)
    # value_bytes - bytes containing TLV value byte(s)
    # returns length (L) byte(s) for TLV

    value_length = len(value_bytes)
    b_value = len_bytes(value_length)
    if value_length < 128:
        return b_value
    else:
        return bytes([SEVEN_BYTES_SHIFT | len(b_value)]) + b_value


def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = b'\xff'
    else:
        bool = b'\x00'
    return bytes([0x01]) + asn1_len(bool) + bool

def asn1_null():
    # returns DER encoding of NULL
    return bytes([NULL | PRIMITIVE | UNIVERSAL]) + bytes([0x00])

def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    der_type = bytes([INTEGER | PRIMITIVE | UNIVERSAL])
    b_value = len_bytes(i)
    if i == 0:
        return der_type + b'\x01' + b'\x00'
    elif i > 0:
        mnb = is_mnb_one(b_value[0])
        length = asn1_len(ZERO_PADDING + b_value) if mnb else asn1_len(b_value)
        return der_type + length + ZERO_PADDING + b_value if mnb else der_type + length + b_value
    pass

def asn1_bitstring(bitstr):
    # bitstr - string containing bitstring (e.g., "10101")
    # returns DER encoding of BITSTRING
    if bitstr == '':
        return bytes([BIT_STRING | PRIMITIVE | UNIVERSAL]) + b'\x01' + b'\x00'

    bitstr_length = len(bitstr)
    missing_bits = bitstr_length - ((bitstr_length >> THREE_SHIFT) << THREE_SHIFT)

    padding = EIGHT_ZERO_BITS_STRING[missing_bits:]
    if missing_bits == 0:
        padding = ''

    bitstr = bitstr + padding
    b_str = [0]*(len(bitstr) >> 3)

    for x in range(len(b_str)):
        value = 0
        for y in range(EIGHT_SHIFT):
            value <<= 1
            if bitstr[(x << 3) + y] == '1':
                value |= 1
        b_str[x] = value

    enc_str = bytes(b_str)
    b_padding = len_bytes(len(padding))

    return bytes([BIT_STRING | PRIMITIVE | UNIVERSAL]) + asn1_len(enc_str + b_padding) + b_padding + enc_str

def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., b"abc\x01")
    # returns DER encoding of OCTETSTRING
    return bytes([OCTET_STRING | PRIMITIVE | UNIVERSAL]) + asn1_len(octets) + octets

def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    object_list = len_bytes(40 * oid[0] + oid[1])

    for value in oid[2:]:
        value_bytes = len_bytes(value, 7)
        last_bit = map(set_bit, value_bytes[: len(value_bytes) - 1])
        object_list += bytes(last_bit) + value_bytes[len(value_bytes) - 1:]
    return bytes([OBJECT_IDENTIFIER | PRIMITIVE | UNIVERSAL]) + asn1_len(object_list) + object_list

def set_bit(value):
    return value | SEVEN_BYTES_SHIFT

def asn1_sequence(der):
    # der - DER bytes to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    return bytes([SEQUENCE | PRIMITIVE | UNIVERSAL]) + asn1_len(der) + der

def asn1_set(der):
    # der - DER bytes to encapsulate into set
    # returns DER encoding of SET
    return bytes([SET | PRIMITIVE | UNIVERSAL]) + asn1_len(der) + der

def asn1_printablestring(string):
    # string - bytes containing printable characters (e.g., b"foo")
    # returns DER encoding of PrintableString
    return bytes([PRINTABLE_STRING | PRIMITIVE | UNIVERSAL]) + asn1_len(string) + string

def asn1_utctime(time):
    # time - bytes containing timestamp in UTCTime format (e.g., b"121229010100Z")
    # returns DER encoding of UTCTime
    return bytes([UTC_TIME | PRIMITIVE | UNIVERSAL]) + asn1_len(time) + time

def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    return bytes([FIVE_RIGHT_BYTES | tag]) + asn1_len(der) + der
#==== ASN1 encoder end ====


MD5 = [1, 2, 840, 113549, 2, 5]
SHA1 = [1, 3, 14, 3, 2, 26]
SHA256 = [2, 16, 840, 1, 101, 3, 4, 2, 1]

def get_algorithm(algorithm):
    if algorithm == MD5:
        return ("HMAC-MD5", hashlib.md5)
    return ("HMAC-SHA1", hashlib.sha1) if algorithm == SHA1 else ("HMAC-SHA256", hashlib.sha256)


def read_asn1_file(filename):
    file = open(filename, "rb")

    decoded = decoder.decode(file.read())

    return bytes(decoded[0][1]), get_algorithm(list(decoded[0][0][0]))

def file_to_digest(filename, key, algorithm=hashlib.sha256):
    file = open(filename, "rb")

    digest = hmac.new(key, None, algorithm)

    while True:
        new_data = file.read(512)
        if not new_data:
            break
        digest.update(new_data)
    return digest

def write_to_file(filename, to_write):
    cipher_file = open(filename, "wb")
    asn1_enc = asn1_sequence(
        asn1_sequence(asn1_objectidentifier(SHA256) + asn1_null()) +
        asn1_octetstring(to_write))
    cipher_file.write(asn1_enc)
    cipher_file.close()

def mac(filename):
    key = input("[?] Enter key: ").encode()

    digest = file_to_digest(filename, key).digest()

    print("[+] Calculated HMAC-SHA256:", digest.hex())

    print("[+] Writing HMAC DigestInfo to", filename+".hmac")
    write_to_file(filename + ".hmac", digest)

def verify(filename):
    print("[+] Reading HMAC DigestInfo from", filename+".hmac")

    digest, algorithm = read_asn1_file(filename + ".hmac")

    # print out the digest
    print(algorithm[0] + " digest: " + digest.hex())
    # ask for the key
    key = input("[?] Enter key: ").encode()
    # print out the calculated HMAC-X digest
    digest_calculated = file_to_digest(filename, key, algorithm[1]).digest()
    print("Calculated " + algorithm[0] + ": " + digest_calculated.hex())
    if digest_calculated != digest:
        print("[-] Wrong key or message has been manipulated!")
    else:
        print("[+] HMAC verification successful!")



def usage():
    print("Usage:")
    print("-mac <filename>")
    print("-verify <filename>")
    sys.exit(1)

if len(sys.argv) != 3:
    usage()
elif sys.argv[1] == '-mac':
    mac(sys.argv[2])
elif sys.argv[1] == '-verify':
    verify(sys.argv[2])
else:
    usage()
