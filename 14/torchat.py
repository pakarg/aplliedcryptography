#!/usr/bin/env python3

# sudo apt install python3-socks
import argparse
import socks
import socket
import sys
import random
# do not use any other imports/libraries

# took 2.5 hours (please specify here how much time your solution required)


# parse arguments
parser = argparse.ArgumentParser(description='TorChat client')
parser.add_argument('--myself', required=True, type=str, help='My TorChat ID')
parser.add_argument('--peer', required=True, type=str, help='Peer\'s TorChat ID')
args = parser.parse_args()

# route outgoing connections through Tor
socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", 9050)
socket.socket = socks.socksocket

# reads and returns torchat command from the socket
def read_torchat_cmd(incoming_socket):
    # read until newline
    cmd = b''

    while True:
        b = incoming_socket.recv(1)

        if b != b'':
            if b == b'\n':
                break
            cmd += b
        else:
            exit()

    # return command
    cmd = cmd.decode('utf-8')

    print("[+] Received: " + cmd)
    return cmd


# prints torchat command and sends it
def send_torchat_cmd(outgoing_socket, cmd):
    outgoing_socket.send(cmd.encode('utf-8') + b'\n')
    print("[+] Sending: " + cmd)


def send_command(message):
    send_torchat_cmd(outgoing_socket, message)


random_bits = str(random.getrandbits(128))
a_peer = args.peer
a_myself = args.myself
sserv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sserv.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sserv.bind(('127.0.0.1', 8888))
waiting_status = True
authenticated = False

# connecting to peer
print("[+] Connecting to peer " + a_peer)
outgoing_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
outgoing_socket.connect((a_peer + '.onion', 11009))

# sending ping
send_command("ping " + a_myself + " " + random_bits)

# listening for the incoming connection
print("[+] Listening...")
sserv.listen(0)
(incoming_socket, address) = sserv.accept()
print("[+] Client %s:%s" % (address[0], address[1]))

# the main loop for processing the received commands
while True:
    cmdr = read_torchat_cmd(incoming_socket)

    cmd = cmdr.split(' ')

    command_type = cmd[0]

    if command_type == 'ping':
        command_peer = cmd[1]
        if command_peer == args.peer:
            send_command("pong " + cmd[2])
    elif command_type == 'pong':
        command_peer = cmd[1]
        if command_peer == random_bits:
            print("[+] Incoming connection authenticated!")
            authenticated = True
    elif command_type == 'status':
        if authenticated and waiting_status:
            send_command("add_me")
            send_command("status available")
            send_command("profile_name Alice")
            waiting_status = False
    elif command_type == 'message':
        if not waiting_status:
            message = input("[?] Enter message: ")
            send_command("message " + message)
