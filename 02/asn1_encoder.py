#!/usr/bin/env python3
import sys   # do not use any other imports/libraries

# took 16.0 hours (please specify here how much time your solution required)
# I have some bug in asn1_bitstring so spent a lot of time searching also had troubles with len_bytes
# asn1_objectidentifier Didn't finish had troubles with time
import traceback

EIGHT_SHIFT = 8
THREE_SHIFT = 3
SEVEN_BYTES_SHIFT = 1 << 7
FIVE_RIGHT_BYTES = 10 << 4
MAX_HEX = 0xFF
ZERO_PADDING = bytes([0x00])
EIGHT_ZERO_BITS_STRING = '00000000'

# Universal Tags
BOOLEAN = 0x01
INTEGER = 0x02
BIT_STRING = 0x03
OCTET_STRING = 0x04
NULL = 0x05
OBJECT_IDENTIFIER = 0x06
ENUMERATED = 0x0a
UTF8_STRING = 0x0c
SEQUENCE = 0x30
SET = 0x31
PRINTABLE_STRING = 0x13
IA5_STRING = 0x16
UTC_TIME = 0x17
UNICODE_STRING = 0x1e

# Classes
UNIVERSAL = 0x00
APPLICATION = 0x40
CONTEXT = 0x80
PRIVATE = 0xc0

# Forms
CONSTRUCTED = 0x20
PRIMITIVE = 0x00


def is_mnb_one(value_bytes):
    return value_bytes & SEVEN_BYTES_SHIFT


def len_bytes(value, shift=EIGHT_SHIFT):
    if value == 0:
        return b'\x00'
    value_bytes = b''
    while value != 0:
        value_bytes = bytes([value & ((1 << shift) - 1)]) + value_bytes
        value = value >> shift
    return value_bytes


def asn1_len(value_bytes):
    # helper function - should be used in other functions to calculate length octet(s)
    # value_bytes - bytes containing TLV value byte(s)
    # returns length (L) byte(s) for TLV

    value_length = len(value_bytes)
    b_value = len_bytes(value_length)
    if value_length < 128:
        return b_value
    else:
        return bytes([SEVEN_BYTES_SHIFT | len(b_value)]) + b_value


def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = b'\xff'
    else:
        bool = b'\x00'
    return bytes([0x01]) + asn1_len(bool) + bool

def asn1_null():
    # returns DER encoding of NULL
    return bytes([NULL | PRIMITIVE | UNIVERSAL]) + bytes([0x00])

def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    der_type = bytes([INTEGER | PRIMITIVE | UNIVERSAL])
    b_value = len_bytes(i)
    if i == 0:
        return der_type + b'\x01' + b'\x00'
    elif i > 0:
        mnb = is_mnb_one(b_value[0])
        length = asn1_len(ZERO_PADDING + b_value) if mnb else asn1_len(b_value)
        return der_type + length + ZERO_PADDING + b_value if mnb else der_type + length + b_value
    pass

def asn1_bitstring(bitstr):
    # bitstr - string containing bitstring (e.g., "10101")
    # returns DER encoding of BITSTRING
    if bitstr == '':
        return bytes([BIT_STRING | PRIMITIVE | UNIVERSAL]) + b'\x01' + b'\x00'

    bitstr_length = len(bitstr)
    missing_bits = bitstr_length - ((bitstr_length >> THREE_SHIFT) << THREE_SHIFT)

    padding = EIGHT_ZERO_BITS_STRING[missing_bits:]
    if missing_bits == 0:
        padding = ''

    bitstr = bitstr + padding
    b_str = [0]*(len(bitstr) >> 3)

    for x in range(len(b_str)):
        value = 0
        for y in range(EIGHT_SHIFT):
            value <<= 1
            if bitstr[(x << 3) + y] == '1':
                value |= 1
        b_str[x] = value

    enc_str = bytes(b_str)
    b_padding = len_bytes(len(padding))

    return bytes([BIT_STRING | PRIMITIVE | UNIVERSAL]) + asn1_len(enc_str + b_padding) + b_padding + enc_str

def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., b"abc\x01")
    # returns DER encoding of OCTETSTRING
    return bytes([OCTET_STRING | PRIMITIVE | UNIVERSAL]) + asn1_len(octets) + octets

def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    object_list = len_bytes(40 * oid[0] + oid[1])

    for value in oid[2:]:
        value_bytes = len_bytes(value, 7)
        last_bit = map(set_bit, value_bytes[: len(value_bytes) - 1])
        object_list += bytes(last_bit) + value_bytes[len(value_bytes) - 1:]
    return bytes([OBJECT_IDENTIFIER | PRIMITIVE | UNIVERSAL]) + asn1_len(object_list) + object_list

def set_bit(value):
    return value | SEVEN_BYTES_SHIFT

def asn1_sequence(der):
    # der - DER bytes to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    return bytes([SEQUENCE | PRIMITIVE | UNIVERSAL]) + asn1_len(der) + der

def asn1_set(der):
    # der - DER bytes to encapsulate into set
    # returns DER encoding of SET
    return bytes([SET | PRIMITIVE | UNIVERSAL]) + asn1_len(der) + der

def asn1_printablestring(string):
    # string - bytes containing printable characters (e.g., b"foo")
    # returns DER encoding of PrintableString
    return bytes([PRINTABLE_STRING | PRIMITIVE | UNIVERSAL]) + asn1_len(string) + string

def asn1_utctime(time):
    # time - bytes containing timestamp in UTCTime format (e.g., b"121229010100Z")
    # returns DER encoding of UTCTime
    return bytes([UTC_TIME | PRIMITIVE | UNIVERSAL]) + asn1_len(time) + time

def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    return bytes([FIVE_RIGHT_BYTES | tag]) + asn1_len(der) + der

# figure out what to put in '...' by looking on ASN.1 structure required (see slides)
asn1 = asn1_tag_explicit(
    asn1_sequence(
        asn1_set(asn1_integer(5) +
                 asn1_tag_explicit(asn1_integer(200), 2) +
                 asn1_tag_explicit(asn1_integer(65407), 11)) +
        asn1_boolean(True) +
        asn1_bitstring("011") +
        asn1_octetstring(b'\x00\x01' + 49*b'\x02') +
        asn1_null() +
        asn1_objectidentifier([1, 2, 840, 113549, 1]) +
        asn1_printablestring(b'hello.') +
        asn1_utctime(b'250223010900Z')), 0)

open(sys.argv[1], 'wb').write(asn1)
